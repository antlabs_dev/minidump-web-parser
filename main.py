#!/usr/bin/env python
#-*- coding: utf-8 -*-

SYMBOLS_DEFAULT_PATH = "./test_app/symbols"
MINIDUMP_STACKWALK_BINARY = "/usr/local/bin/minidump_stackwalk"
SERVER_HOST = "0.0.0.0"
SERVER_PORT = 8888

from bottle import route, run, template, request
from subprocess import Popen, PIPE
from tempfile import NamedTemporaryFile
from time import time
from sys import argv
from os import path, chdir, remove
from tarfile import TarFile
from threading import Thread

def get_symbols_path():
    symbols_path = SYMBOLS_DEFAULT_PATH
    if len(argv) > 1:
        symbols_path = argv[1]
    return symbols_path

def extract_and_remove_symbols_archive(archive, extract_path):
    arch = TarFile.open(archive, "r:gz")
    arch.extractall(extract_path)
    print "file extacted:", archive, "to:", extract_path
    remove(archive)

@route("/")
def index():
    return template("index.html")

@route("/dump_upload", method="POST")
def dump_upload():
    start_time = time()
    upload = request.files.get("upload")
    enable_stderr = request.forms.get("enable_stderr")
    temp_file = NamedTemporaryFile(suffix=upload.filename)
    temp_file.write(upload.file.read())
    print "crashdump uploaded:", temp_file.name
    symbols_path = get_symbols_path()
    minidump_process = Popen([MINIDUMP_STACKWALK_BINARY, temp_file.name, symbols_path], stdout=PIPE, stderr=PIPE)
    out, err = minidump_process.communicate()
    end_time = time()
    generate_time = end_time - start_time
    print "stacktrace generated in", generate_time, "seconds"
    return template(
                    "stacktrace.html", 
                    out=out, err=err if enable_stderr is not None else "",
                    filename=upload.filename, 
                    generate_time=generate_time, 
                    symbols_path=symbols_path
                    )

@route("/symbols_upload", method="POST")
def symbols_upload():   
    upload = request.files.get("symbols_upload")
    temp_file = NamedTemporaryFile(delete=False, suffix=".tar.gz")
    temp_file.write(upload.file.read())
    print "archived symbols uploaded:", temp_file.name
    Thread(target=extract_and_remove_symbols_archive, args=(temp_file.name, get_symbols_path())).start()
    return """<!DOCTYPE html>
                <html>
                    <head>
                        <meta charset="UTF-8">
                        <title>Crash reports parser</title>
                    </head>
                    <body>
                       <h1>OK</h1>
                       <a href=/>Back to index page</a>
                    </body>
                </html>"""

def main():
    abspath = path.abspath(__file__)
    dname = path.dirname(abspath)
    chdir(dname)
    if len(argv) <= 1:
        print "WARNING! No symbols path given. Using default", SYMBOLS_DEFAULT_PATH, \
        "To specify debug symbols path, run", argv[0], "<path/to/symbols"
    else:
        print "Using symbols path", argv[1]
        
    run(host=SERVER_HOST, port=SERVER_PORT)

if __name__ == "__main__":
    main()