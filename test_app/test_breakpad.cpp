#include <cstdio>
#include <ctime>

#include "client/linux/handler/exception_handler.h"

static bool dumpCallback(const google_breakpad::MinidumpDescriptor& descriptor,void* context, bool succeeded)
{
    const char *path = descriptor.path();
    printf("Dump path: %s\n", path);
    char newpath[512] = {0};
    strcat(newpath, descriptor.directory().c_str());
    strcat(newpath, "/crash_dump_");
    time_t t = time(NULL);
    struct tm *now = localtime(&t);
    char buf[256] = {0};
    strftime(buf, sizeof buf, "%H_%M_%S_%Y%m%d", now);
    strcat(newpath, buf);
    strcat(newpath, ".dmp");
    rename(path, newpath);
    printf("Renamed to: %s\n", newpath);
    return succeeded;
}

void crash()
{
      volatile int *a = (int *)(NULL);
      *a = 1;
}

int main(int argc, char* argv[])
{
    google_breakpad::MinidumpDescriptor descriptor("/tmp");
    google_breakpad::ExceptionHandler eh(descriptor, NULL, dumpCallback, NULL, true, -1);
    crash();
    return 0;
}
